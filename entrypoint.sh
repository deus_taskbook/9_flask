#!/bin/sh

set -e

sleep 20s
flask db upgrade
python seed.py

gunicorn app:app -b 0.0.0.0:8000
