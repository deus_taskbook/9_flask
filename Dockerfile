FROM python:3.9-slim

WORKDIR /app

RUN apt-get update && apt install -y\
  libpq-dev\
  gcc\
  && rm -rf /var/lib/apt/lists/*

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .

EXPOSE 8000

ENTRYPOINT [ "/app/entrypoint.sh" ]
